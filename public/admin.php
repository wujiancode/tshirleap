<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

// [ 后台入口文件 ]
if (version_compare(PHP_VERSION, '5.4.0', '<')) exit('PHP版本至少要5.4！！');

/**
 * 开启调试模式
 * 项目部署后关闭调试模式
 */
// define('app_debug', true);
// 绑定当前后台入口文件到Admin模块
// define('BIND_MODULE', 'admin');
// define('auto_bind_module', true); // 自动入口绑定，只要入口文件都是对应实际的模块名

// WEB部署目录
define('PUBLIC_PATH', __DIR__ . '/'); 
// 定义静态文件目录
define('STATIC_PATH', __DIR__ . '/static/'); 
// 定义模板目录
define('TEMPLATE_PATH', __DIR__ . '/template/'); 
// 定义应用目录
define('APP_PATH', __DIR__ . '/../application/');

// 加载框架引导文件
require __DIR__ . '/../thinkphp/start.php';
