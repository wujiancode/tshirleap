<?php
/**
 * 模板配置
 */
return [
    'template' => [
        // 全局模板路径
        'view_base'    => PUBLIC_PATH . 'template',
    ],
];