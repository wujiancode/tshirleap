<?php
namespace app\common\behavior;

use think\Lang;
use think\Route;

class AdminInitBehavior
{
    function run() 
    {
        // 注册验证码路由
        Route::get('new_captcha', "@common/captcha_new/index");

        $root = str_replace('//', '/', request()->root());
        // 去掉admin.php及后面的字符串
        $root = preg_replace('/(?=\S+)\w+\.php\S*/i', '', $root);

        $view_replace_str = [
            '__ADMINLTE__' => $root . 'static/AdminLTE',
        ];
        config('view_replace_str', $view_replace_str);
        // 配置当前请求的模块名、控制器名和操作名
        $dispatch = request()->dispatch();
        if (isset($dispatch['module']))
        {
            define('MODULE', str_camel_to_lower($dispatch['module'][0]));
            define('CONTROLLER', str_camel_to_lower($dispatch['module'][1]));
            define('ACTION', str_camel_to_lower($dispatch['module'][2]));
        }
       
        // 配置当前语言
        define('LANGUAGE', Lang::detect());
    }
}