<?php
namespace app\common\behavior;

use think\Route;

class InitHookBehavior
{
    public function run()
    {
        Route::get('new_captcha', "@common/captcha_new/index");
    }
}