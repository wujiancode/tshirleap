<?php
/**
 * 自定义标签
 */
namespace app\common\taglib;

use think\template\TagLib;

class Tr extends TagLib
{
    /**
     * 定义标签列表
     */
    protected $tags   =  [
        // 标签定义： attr 属性列表 close 是否闭合（0 或者1 默认1） alias 标签别名 level 嵌套层次
        'captcha' => ['attr' => 'height,width', 'close' => 0],
    ];

    /**
     * 验证吗标签
     */
    public function tagCaptcha($tag)
    {
        $height    = empty($tag['height']) ? '' : '&height=' . (int)$tag['height'];
        $width     = empty($tag['width']) ? '' : '&width=' . (int)$tag['width'];
        $font_size = empty($tag['font-size']) ? '' : '&font_size=' . (int)$tag['font-size'];
        $nth       = empty($tag['nth']) ? '' : '&nth=' . (int)$tag['nth'];
        $id        = empty($tag['id']) ? '' : $tag['id'];
        $length    = empty($tag['length']) ? '' : '&length=' . (int)$tag['length'];
        $style     = empty($tag['style']) ? 'cursor: pointer;' : $tag['style'];
        $title     = empty($tag['title']) ? '点击换一张' : $tag['title'];
        $ref       = empty($tag['ref']) ? '' : $tag['ref'];

        $params    = $height . $width . $font_size . $nth . $length;
        $params    = ltrim($params, '&');
        $captcha_src = url('/new_captcha') . '?' . $params;
        $parse = <<<parse
        <img src="{$captcha_src}" alt="captcha" onclick="this.src='{$captcha_src}&randrom=' + Math.random();" title="{$title}" id="{$id}" ref="{$ref}" style="{$style}">
parse;
        return $parse;
    }
}