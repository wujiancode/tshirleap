<?php
namespace app\common\validate;

use think\Validate;

class Base extends Validate
{
    protected $message = [
        'captcha.require'        => '{%CAPTCHA_ERROR}',
        'captcha.captcha'        => '{%CAPTCHA_ERROR}',
        'captcha.captcha'        => '{%CAPTCHA_ERROR}',
    ];
    
    /**
     * 验证某个字段的值是否至少包含字母和数字
     * @param string $value 要验证的字段值
     * @param string $rule 验证规则
     * @return bool 验证成功返回true,否则返回false
     */
    protected function alphaNumAnd($value, $rule)
    {
        return preg_match('/^(?=.*\d)(?=.*[a-zA-Z])[\da-zA-Z~!@#$%^&*()><,.?\-_=+\/|;:\'"]{2,}$/', $value) ? true : false;
    }
}