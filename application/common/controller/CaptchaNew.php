<?php
/**
 * 验证码生成
 */
namespace app\common\controller;

use think\captcha\Captcha;
use think\Request;

class CaptchaNew
{
    public function index(Request $request)
    {
        $config =    [
            // 验证码字体大小(px)
            'fontSize' => 25,    
            // 验证码位数
            'length'   => 5,   
            // 画混淆曲线
            'useCurve' => true,
            // 关闭验证码杂点
            'useNoise' => false, 
            // 验证码图片高度，设置为0为自动计算
            'imageH'   => 0,
            // 验证码图片宽度，设置为0为自动计算
            'imageW'   => 0
        ];
        
        $fontSize = $request->param('font_size', $config['fontSize'], 'intval');
        if ($fontSize > 8 && $fontSize < 50)
        {
            $config['fontSize'] = $fontSize;
        }

        $length = $request->param('length', $config['length'], 'intval');
        if ($length > 3 && $length < 15)
        {
            $config['length'] = $length;
        }

        $height = $request->param('height', $config['imageH'], 'intval');
        if ($height > 10 && $height < 100)
        {
            $config['imageH'] = $height;
        }

        $width = $request->param('width', $config['imageW'], 'intval');
        if ($width > 10 && $width < 200)
        {
            $config['imageW'] = $width;
        }

        $id = $request->param('id', '', 'intval');
        
        // 输出图片前不能有其他输出
        // @ob_clean();
        // $captcha = new Captcha($config);
        // return $captcha->entry($id);
        return captcha($id, $config);
    }
}