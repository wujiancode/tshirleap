<?php
/**
 * 规则表
 */
namespace app\admin\model;

use app\common\model\Base;

class AuthRule extends Base
{
    // 设置返回数据集的对象名
    protected $resultSetType = 'collection';
    /**
     * 定义与后台菜单表关联的方法
     */
    public function adminMenus()
    {
        return $this->hasOne('AdminMenus', 'url', 'name');
    }
}