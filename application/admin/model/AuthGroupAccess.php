<?php
/**
 * 用户组明细表管理
 * 
 * 与其他表关联操作
 */
namespace app\admin\model;

use app\common\model\Base;

class AuthGroupAccess extends Base
{
    /**
     * 一对一关联AuthGroup模型
     */
    function authGroup()
    {
        // return $this->hasOne('AuthGroup', 'uid');
    }
}