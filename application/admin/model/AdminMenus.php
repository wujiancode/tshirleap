<?php
/**
 * 后台菜单表
 */
namespace app\admin\model;

use app\common\model\Base;

class AdminMenus extends Base
{
    /**
     * 获取给定父ID下的子菜单
     * 
     * @param int $pid 父ID
     * @return array 返回子菜单
     */
    function getSubAdminMenus($pid)
    {
        $menus = $this->where('pid', '=', $pid)->select();
        return $menus;
    }
}