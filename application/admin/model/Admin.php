<?php
namespace app\admin\model;

use app\common\model\Base;

class Admin extends Base
{
    protected $autoWriteTimestamp = true;
    
    // 定义更新时间戳字段名
    protected $updateTime = 'last_modified';

    // protected $auto = ['last_login_time', 'last_login_ip'];
    protected $insert = [
        'last_login_time', 
        'last_modified',
        'last_login_ip'
    ]; 

    // 设置最近登录时间为当前时间
    protected function setLastLoginTimeAttr($value)
    {
        return time();
    }

    // 设置最近登录IP为当前访问IP
    protected function setLastLoginIpAttr($value)
    {
        return request()->ip();
    }
    /**
     * 获取管理员用户数据
     * 
     * @param string $name 用户名或用户邮箱地址字段名
     * @param string $value 用户名或用户邮箱地址字段值
     * @return object 返回用户结果集对象
     */
    function getAdmin($name, $value)
    {
        $admin = $this->where($name, '=', $value)->find();

        return $admin;
    }

    /**
     * 更新最近登录时间和最近登录IP
     * 
     * @param string|integer $id 用户ID
     * @return integer|false 成功返回更新条数，失败返回false
     */
    function update_last_time_ip($id)
    {
        $auto = ['last_login_time', 'last_login_ip'];
        $this->auto($auto)->save([], ['id' => $id]);
    }

    /**
     * 一对一关联AuthGroupAccess模型
     */
    function authGroupAccessOne()
    {
        return $this->hasOne('AuthGroupAccess', 'uid');
    }

    /**
     * 一对多关联AuthGroupAccess模型
     */
    function authGroupAccessMany()
    {
        return $this->hasMany('AuthGroupAccess', 'uid');
    }

    /**
     * 给定管理员ID，获取管理员所属的用户组
     * 
     * @param int|string $id 管理员ID
     * @return object|false
     */
    function getAdminGroup()
    {
        $AuthGroup_model = model('AuthGroup');
        // $Admin_result = $AuthGroup_model->where('id', 'IN', function($query) use($id) {
        //     $query->name('auth_group_access')->where('uid', $id)->field('group_id');
        // })->find();


        
        // return $Admin_result;
    }
}