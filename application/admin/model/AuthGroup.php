<?php
/**
 * 用户组表管理
 * 
 */
namespace app\admin\model;

use app\common\model\Base;

class AuthGroup extends Base
{
    protected $autoWriteTimestamp = true;
    // 设置返回数据集的对象名
    protected $resultSetType = 'collection';
    // 设置新增时rules赋空值
    protected $insert = ['rules' => ''];
}