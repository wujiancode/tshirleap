<?php
namespace app\admin\validate;

use app\common\validate\Base;

class Admin extends Base
{
    protected $rule = [
        'admin_name|用户名'   => 'require|alphaDash',
        'admin_pass|密码'     => 'require|length:6,20|alphaNumAnd',
        'captcha|验证码'      => 'require|captcha',
        'group_id|角色组'     => 'integer',
        'admin_email|邮箱'    => 'email',
        'icon|头像'           => 'file|image|fileExt:jpg,jpeg,png,gif,bmp,svg,wmf,webp|fileSize:' . (1024*1024*2),
        'admin_nickname|昵称' => 'chsDash|length:2,20',
        'status|状态'         => 'integer|in:0,1'

    ];

    protected $message = [
        'admin_name.require'     => '{%USERNAME_ERROR}',
        'admin_name.token'       => '{%TOKEN_ERROR}',
        'admin_pass.require'     => '{%PASSWORD_ERROR}',
        'admin_pass.length'      => '{%PASSWORD_ERROR}',
        'admin_pass.alphaNumAnd' => '{%PASSWORD_ERROR}',
    ];

    // 不能给场景的字段加别名
    protected $scene = [
        'disable_verify' => ['username', 'password'],
        // 管理员编辑场景
        'edit' => [
            'admin_name'  => 'require|length:4,20|alphaDash|token',
            'admin_pass'  => 'length:6,20|alphaNumAnd',
            'icon',
            'admin_nickname',
            'admin_email' => 'require|email',
            'status'      => 'require|integer|in:0,1',
        ],
        // 管理员新增场景
        'create' => [
            'admin_name'  => 'require|length:4,20|alphaDash|unique:admin|token',
            'admin_pass'  => 'length:6,20|alphaNumAnd|confirm',
            'icon',
            'admin_nickname',
            'admin_email' => 'require|email',
            'status'      => 'require|integer|in:0,1'
        ]
    ];
}