<?php
namespace app\admin\validate;

use think\Validate;

class AdminMenus extends Validate
{
    protected $rule = [
        'id'     => 'require',
        'pid'    => 'require|number',
        'title'  => 'require',
        'url'    => 'require',
        'status' => 'number',
    ];

    protected $message = [
        'id.require'    => '{%MENU_ID_EMPTY_ERROR}',
        'pid.require'   => '{%MENU_PID_EMPTY_ERROR}',
        'pid.number'    => '{%MENU_PID_NUMBER_ERROR}',
        'title.require'  => '{%MENU_NAME_ERROR}',
        'url.require'   => '{%MENU_URL_ERROR}',
        'status.number' => '{%MENU_STATUS_ERROR}',
    ];

    protected $scene = [
        // 菜单编辑场景
        'edit' => ['id', 'pid', 'title', 'url', 'status'],
    ];
}