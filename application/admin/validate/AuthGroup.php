<?php
namespace app\admin\validate;

use think\Validate;

class AuthGroup extends Validate
{
    protected $rule = [
        'id|角色ID'      => 'require|integer',
        'title|角色名称' => 'require|chsDash',
        'status|状态'    => 'require|integer'
    ];

    protected $message = [];

    protected $scene = [
        // 更新场景
        'edit' => [],
        // 新增场景
        'create' => [
            'title|角色名称' => 'require|chsDash',
            'status|状态'    => 'require|integer'
        ],
    ];
}