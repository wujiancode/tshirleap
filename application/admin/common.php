<?php

use Library\ORG\Util\Auth;

/**
 * 冒泡排序算法
 */
function bubbleSort(array &$array)
{
    $len = count($array);
    for ($i=1; $i < $len; $i++) {
        for ($j=0; $j < ($len-$i); $j++) {
            $before = $array[$j];
            $after = $array[$j+1];
            if ($before > $after)
            {
                $array[$j] = $after;
                $array[$j+1] = $before;
            }
        }
    }
}

/**
 * 快速排序之双路排序算法
 */
function twoWayForQuickSort(array &$array, $left = 0, $right = null)
{
    if (null === $right)
    {
        $right = 
    }
}

/**
 * 检查是否启用后台登录验证码
 * @return string 返回0表示后台登录禁用验证码，
 * 返回1表示后台登录启用验证码。
 */
function tr_enabled_admin_captcha()
{
    $captcha = cache('captcha');
    if (!$captcha) 
    {
        $config = model('Config');
        $captcha = $config->where([
            'config_name'  => 'admin_captcha',
        ])->find();

        cache('captcha', $captcha);
    }

    return $captcha->config_value;
}

/**
 * 过滤邮箱地址
 * 
 * 如果不是email地址，则不过滤
 * 
 * @return string 如果邮箱地址不合法，则返回空字符串，
 * 否则原样返回
 */
// function tr_filter_email($email)
// {
//     if (false !== strpos($email, '@'))
//     {
//         $email = filter_var($email, FILTER_VALIDATE_EMAIL);
//         $email = $email ?: '';
//     }
    
//     return $email;
// }

/**
 * 检查管理员是否已登录
 * 
 * @return bool 返回true则已登录，false为未登录
 */
function tr_check_admin_logged()
{
    return session('?admin');
}

/**
 * 检查当前管理员是否有权修改管理员用户信息
 * 
 * 超管员没有限制，普管员不能改其他任何人，也不能改自己的用户组
 * 
 * @param int $admin_id 要检查的管理员ID
 * @param array $field 要检查的管理员字段，实际是用户组['group_id'=>$value]
 * @return true|string 返回true则管理员有操作权力，否则返回错误消息
 */
function check_admin_is_authorized($admin_id, $field = [])
{
    $error_msg =  '';
    // 非超管员修改用户信息受限
    if (1 != session('admin.group_id'))
    {
        // 普管员不能改其他管理员
        if ($admin_id != session('admin.id')) 
        {
            $error_msg = '非超管员无权修改其他管理员信息';
        } else {
            if (array_key_exists('group_id', $field))
            {
                // 普管员不能改自己角色组
                if ($field['group_id'] != session('admin.group_id')) 
                {
                    $error_msg = '非超管员无权修改角色组';
                }
            }
        }
    }

    return $error_msg ?: true;
}

/**
 * 后台菜单数据递归成多维数组，格式如下所示：
 * array(
 *     item1,
 *     ...,
 *     'children'=>array(
 *         item1,
 *         ..,
 *         'children'=>...
 *     )
 * )
 * 另外把id值从key移到item里(例如item:['id'=>value])
 * 
 * @param array $data 要处理的数据
 * @param array $checked 对应$data的checked
 * @param array $status 对应$data的disable
 * @param int $parent_k [optional] 从这个节点开始
 * @return array
 */
function menu_data_recursive($data, $checked, $status, $parent_k = 0)
{
    $array = [];
    foreach ($data[$parent_k] as $sub_k => $item) 
    {
        $item['id'] = $sub_k;
        // 判断菜单项是否在用户组规则里
        // 如果有，则在菜单数组的对应项设置“选中”值
        if ( in_array($item['href'], $checked) ) 
        {
            $item['checked'] = 1;
        } else {
            $item['checked'] = 0;
        }
        // 判断菜单对应的规则状态值为1则规则禁用，值为0则启用
        $item['disabled'] = (0 === $status[$item['href']]) ? true : false;  
        // 如果有子级的话，则递归子级
        if ( isset($data[$sub_k]) )
        {
            $item['children'] = menu_data_recursive($data, $checked, $status, $sub_k);
        }   
        $array[] = $item;
    }
    return $array;
}

/**
 * 递归获取$data中有元素checked=1的所有菜单ID
 * 
 * @param array $data 要递归的数组变量
 * @param int 数组深度
 * @return string
 */
function get_deepest_id_checked(array $data, $depth = 0)
{
    $result = '';
    foreach ($data as $item) 
    {
        // 获取本级选中的ID
        if (1 === $item['checked']) $result .= $item['id'] . ',';
        if ( isset($item['children']) )
        {
            // 获取子级选中的ID
            $result .= get_deepest_id_checked($item['children'], 1+$depth);
        }
    }
    // 最后把','尾巴去掉
    if ( 0 === $depth ) $result = rtrim($result, ',');
    return $result;
}

/**
 * 返回$data数组的href元素，并且是键出现在$menus_id的部分
 * 
 * @param array $data 菜单数据
 * @param array $menus_id 菜单ID，指定要从$data中返回的部分
 * @return string 由$data的href键的值连成的字符串
 */
function get_href_checked(array $data, array $menus_id, $parent_k = 0, $depth = 0)
{
    $result = '';
    foreach ($data[$parent_k] as $sub_k => $item) 
    {
        if ( in_array($sub_k, $menus_id) )
        {
            $result .= $item['href'] . ',';
        }
        if ( isset($data[$sub_k]) )
        {
            $result .= get_href_checked($data, $menus_id, $sub_k, 1+$depth); 
        }
    }
    // 最后把','尾巴去掉
    if ( 0 === $depth ) $result = rtrim($result, ',');
    return $result;
}

/**
 * 检查权限规则是否在访问白名单中
 * 
 * @param string $rule 权限规则格式"控制器/方法"
 * @return bool
 */
function in_access_whitelist($rule = '')
{
    // 定义访问白名单 
    $allow_access = [
        'Index/index',      // 首页
        'Login/index',      // 登录页
        'Login/doLogin',    // 登录验证
        'Index/logout'      // 注销 
    ];
    if ( empty($rule) ) $rule = str_lower_to_camel(CONTROLLER) . '/' . ACTION;
    // 检查权限规则是否在访问白名单中
    if ( in_array($rule, $allow_access) )
    {
        return true;
    } else {
        return false;
    }
}

/**
 * 检查权限规则
 * 
 * @param string|array $rule 权限规则
 * @param int $uid 用户ID
 * @return bool
 */
function check_auth_rule($rule, $uid)
{
    static $auth = null;

    if (!$auth) $auth = new \Library\ORG\Util\Auth();
    
    if ( $auth->check($rule, $uid) )
    {
        return true;
    } else {
        return false;
    }
}

/**
 * 获取系统信息
 */
function get_system_info()
{
    $info = [
        'url'             => $_SERVER['HTTP_HOST'],
        'document_root'   => $_SERVER['DOCUMENT_ROOT'],
        'server_os'       => PHP_OS,
        'server_port'     => $_SERVER['SERVER_PORT'],
        'server_ip'       => isset($_SERVER['SERVER_ADDR']) ? $_SERVER['SERVER_ADDR'] : '',
        'server_soft'     => $_SERVER['SERVER_SOFTWARE'],
        'mysql_version'   => db()->query('SELECT VERSION()')[0]['VERSION()'],
        'version'         => THINK_VERSION,
        'max_upload_size' => ini_get('upload_max_filesize'),
    ];
    return $info;
}

/**
 * 系统信息转表格格式
 */
function sysinfo_to_table()
{
    $info = get_system_info();
    $table = [];
    $i = 0;
    foreach ($info as $k => $v)
    {
        $table[$i]['value'] = $v;
        switch ($k) {
            case 'url':
                $table[$i]['name'] = '网站域名';
                break;
            case 'document_root':
                $table[$i]['name'] = '网站根目录';
                break;
            case 'server_os':
                $table[$i]['name'] = '服务器操作系统';
                break;
            case 'server_port':
                $table[$i]['name'] = '服务器端口';
                break;
            case 'server_ip':
                $table[$i]['name'] = '服务器IP';
                break;
            case 'server_soft':
                $table[$i]['name'] = '网站运行环境';
                break;
            case 'mysql_version':
                $table[$i]['name'] = 'MySQL版本';
                break;
            case 'version':
                $table[$i]['name'] = 'ThinkPHP版本';
                break;
            case 'max_upload_size':
                $table[$i]['name'] = '最大上传大小';
                break;
            
            default:
                $table[$i]['name'] = '';
                $table[$i]['value'] = '';
                break;
        }
        $i++;
    }
    return $table;
}