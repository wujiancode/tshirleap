<?php
/**
 * 负责角色组的业户部分
 */
namespace app\admin\logic;

class AuthGroup
{
    /**
     * 表单数据
     */
    private $data = [];

    /**
     * 验证表单数据
     * 
     * @param string $scene 验证场景
     * @param array $rules 验证规则
     * @return array 返回错误消息
     */
    public function formValidation(string $scene = '', array $rules = [])
    {
        $data        = $this->getFormData();
        $msg_result  = [];
        // 将消息结果初始化为成功消息
        add_message($msg_result, '', '更新成功！', 'success');

        $validate = validate('AuthGroup');
        
        // 检查表单验证
        if ( !$validate->batch()->check($data, $rules, $scene) )
        {
            add_message($msg_result, $validate->getError(), '更新失败！');
        }

        return $msg_result;
    }

    /**
     * 获取表单数据
     */
    public function getFormData($part = 'form')
    {
        if ( empty($this->data) )
        {
            $formData = json_decode(input($part), true);
    
            foreach ($formData as $key => $value) {
                if ( isset($formData[$key]) ) $this->data[$key] = $value;
            }
        }
        return !empty($this->data) ? $this->data : false;
    }

    /**
     * 检查管理员是否有角色组状态修改权限
     * 
     * @return array 
     */
    public function checkStatusPermission($id, $status, array $msg = [], $title = '')
    {
        if (1 == $id)
        {
            // 超管组的状态，谁都不能禁用
            if (0 == $status) 
                add_message($msg, '超管组的状态不允许禁用', $title);
        } else {
            // 只有超管员可以改普管组
            if (1 != session('admin.group_id'))
                add_message($msg, '普通管理员无权修改普管组的状态', $title);
        }
        return $msg;
    } 

    /**
     * 获取与规则ID对应的菜单ID
     */
    public function ruleidToMenuid($ruleids)
    {
        // 获取规则表的name字段数据
        $adminMenusModel = model('AdminMenus');

    }  
}