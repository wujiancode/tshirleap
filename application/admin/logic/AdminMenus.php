<?php
/**
 * 处理后台菜单表数据
 */
namespace app\admin\logic;

use app\common\model\Base;
use think\Collection;

class AdminMenus extends Base
{
    /**
     * 获取后台菜单表数据
     * 
     * @param int $status 按status获取，值为1、0和-1，分别获取状态为1、0和所有的菜单
     * @return array
     */
    function getMenuData($status = -1)
    {
        $admin_menu = model('AdminMenus');
        $result = $admin_menu->getAllData();
        $menus = [];
        // 将结果集转换成CategoriesGeneral类所需的数据格式
        foreach ($result as $item)
        {
            if ( (-1 != $status) && ($status != $item['status']) ) continue;
            $menus[$item['pid']][$item['id']] = array(
                'name'   => $item['title'],
                'href'   => $item['url'],
                'icon'   => $item['icon'],
                'sort'   => $item['sort'],
                'status' => $item['status'],
                'params' => $item['params'],
            );
        }
        return $menus;
    }
}