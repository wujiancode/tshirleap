<?php
namespace app\admin\logic;

use app\admin\model\Admin as ModelAdmin;
use think\Db;
use think\Lang;
use think\Session;

class Admin extends ModelAdmin
{
    use \traits\controller\Jump;
    
    /**
     * 用户登录认证
     */
    function checkLogin()
    {
        $password = input('password');
        $username = input('username');
        // 如果用email登录，则验证email的合法性
        // 确保可以通过用户名或email地址登录
        if (false !== strpos($username, '@'))
        {
            $username = input('username', '', FILTER_VALIDATE_EMAIL);
            $name_k = 'admin_email';
        } else {
            $name_k = 'admin_name';
        }
        $name_v = $username;

        $data = [
            'admin_name' => $username,
            'admin_pass' => $password,
            'captcha'    => input('captcha'),
        ];

        $scene = '';
        // 如果关闭后台登录验证，则登录时不验证验证码
        if ('0' == tr_enabled_admin_captcha())
        {
            $scene = 'disable_verify';
        }
        
        // 验证用户登录表单填写是否正确
        $validate = validate('Admin');
        if (!$validate->scene($scene)->check($data))
        {
            $this->error($validate->getError());
        }
        
        $admin_model = model('Admin');
        // 获取登录用户信息
        $admin = $admin_model->getAdmin($name_k, $name_v);
        // 如果用户不存在或密码不正确
        if (empty($admin->id) || !password_verify($password, $admin->admin_pass))
        {
            $this->error(lang('USERNAME_PASSWORD_ERROR'));
        } // 如果用户是禁用状态
        elseif (1 != $admin->status) {
            $this->error(lang('ACCOUNT_DISABLE'));
        }

        $group_access = Db::name('auth_group_access')
            ->field('group_id')
            ->where('uid', '=', $admin->id)
            ->find();

        // 更新最近登录时间和最近登录IP
        $admin_model->update_last_time_ip($admin->id);

        // 防止session攻击
        // session_regenerate_id();
        Session::regenerate();

        // 保存登录用户的信息到会话
        session('admin', [
            'id'         => $admin->id,
            'name'       => $admin->admin_name,
            'email'      => $admin->admin_email,
            'nickname'   => $admin->admin_nickname,
            'login_time' => $admin->last_login_time,
            'login_ip'   => $admin->last_login_ip,
            'icon'       => $admin->icon,
            'status'     => $admin->status,
            'group_id'   => $group_access['group_id']
        ]);
        // 记录登录成功事件到日志

        // 用户登录成功跳转到后台首页
        $this->success(lang('LOGIN_SUCCESS'), 'admin/index/index');
    }  

    /**
     * 更新管理员用户信息
     * 
     * @param array $id 用户ID
     * @param array $data 要更新的数据
     * @param array|null $rule 验证规则
     * @param array $msg — 提示信息
     * @param true|false $batch 批量验证
     * @return array 更新失败返回错误消息，当$batch为true则返回一个错误信息的数组
     */
    function updateAdmin($id, $data, $rule = null, $msg = [], $batch = false)
    {
        $admin = $this->validate($rule, $msg, $batch)->editData($data, $id);
        $result = [];
        // 更新失败返回fale和消息
        if (false === $admin)
        {
            $result = [
                'errorMsg' => $this->getError(),
                'error' => true
            ];
        } else if ($admin) {
            $result = [
                'errorMsg' => '',
                'error' => false
            ];
        }
    
        return $result;
    }
}