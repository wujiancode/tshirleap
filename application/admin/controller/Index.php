<?php
namespace app\admin\controller;

/**
 * 后台首页控制器
 */
class Index extends AdminBase
{
    /**
     * 后台首页
     */
    public function index()
    {
        // 获取系统信息
        $sys_info = sysinfo_to_table();
        $this->assign( 'sys_info', json_encode($sys_info, JSON_UNESCAPED_UNICODE) );
        return $this->fetch();
    }

    public function test()
    {
        var_dump(url('/', [], false, true));
    }

    /**
     * 退出当前用户
     */
    public function logout()
    {
        // 可以先跳转到退出确认页，参考WP
        session('admin', null);
        $this->redirect('login/index');
    }
}
