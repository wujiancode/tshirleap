<?php
/**
 * 管理员管理
 * 
 * 管理员新增、编辑、删除
 */
namespace app\admin\controller;

use Think\Db;

class Admin extends AdminBase
{
    /**
     * 管理员列表展示
     */
    function index()
    {

        return $this->fetch();
    }

    /**
     * Ajax请求时返回JSON格式的用户信息
     */
    function user_info_json()
    {
        $result = Db::name('admin')
        ->field('a.id, a.admin_email, a.admin_nickname, 
                 FROM_UNIXTIME(a.last_login_time, \'%Y-%m-%d %H:%i:%s\') as last_login_time, 
                 a.last_login_ip, a.icon, a.status, ag.title')
        ->alias('a')
        ->join('auth_group_access aga','a.id = aga.uid', 'LEFT')
        ->join('auth_group ag','ag.id = aga.uid', 'LEFT')
        ->order('a.id')
        ->select();
        
        if (empty($result))
        {
            $this->error();
        } else {
            $this->success('', '', $result);
        }
    }

    function xiugai() {
        $this->success('ffff', '', ['aaa'=>'bbb'], 1);
    }
}