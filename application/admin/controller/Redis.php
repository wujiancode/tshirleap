<?php
/**
 * 专为各种测试用的
 */
namespace app\admin\controller;

use Predis\Client;
use think\Cache;
use think\Request;

class Redis extends AdminBase
{
    function index(Request $request)
    {
        $user_name = [
            'user:1:name' => 'Tomm',
            'user:2:name' => 'Jack'
        ];
        var_dump(
            Cache::store('redis')->mset($user_name)->getPayload(),
            Cache::store('redis')->mget(array_keys($user_name))
        );
        // return $this->fetch();
    }
}