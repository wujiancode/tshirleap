<?php
/**
 * 管理员管理
 * 
 * 管理员查询、新增、编辑、删除
 */
namespace app\admin\controller;

use Symfony\Component\Finder\Finder;
use think\Request;
use think\Db;

class Admins extends AdminBase
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index(Request $request)
    {
        // Ajax请求时返回管理员列表
        if ($request->isAjax())
        {
            $user = Db::name('admin')
                ->field('a.id, a.admin_email, a.admin_nickname, a.last_login_time, 
                        a.last_login_ip, a.icon, a.status, ag.title, ag.id as ag_id')
                ->alias('a')
                ->join('auth_group_access aga','a.id = aga.uid', 'LEFT')
                ->join('auth_group ag','ag.id = aga.group_id', 'LEFT')
                ->order('a.id')
                ->select();

            $authGroupModel = model('AuthGroup');
            $authGroup = $authGroupModel->getAllData();

            $finder = new Finder();
            // 获取目录下的所有头像文件路径
            $finder->files()->in(STATIC_PATH . 'AdminLTE/dist/img/avatars/')->name('/^.+\.(png|jpg|gif)$/');
            // 获取头像文件名
            foreach ($finder as $file)
            {
                $ext = $file->getExtension();
                $avatars[] = [relative_file_path($file->getRealPath()) => $file->getBasename('.' . $ext)];
            }
            $result = [
                'user'      => $user,
                'authGroup' => $authGroup,
                'avatars'   => $avatars
            ];
            if (empty($result))
            {
                $this->error();
            } else {
                $this->success('', '', $result);
            }            
        }
        return $this->fetch();
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        // 获取用户-角色组列表
        $auth_group_model = model('AuthGroup');
        $auth_group = $auth_group_model->getAllData();

        $finder = new Finder();
        // 获取目录下的所有头像文件路径
        $finder->files()->in(STATIC_PATH . 'AdminLTE/dist/img/avatars/')->name('/^.+\.(png|jpg|gif)$/');
        // 获取头像文件名
        foreach ($finder as $file)
        {
            $ext = $file->getExtension();
            $avatars[] = [
                'name' => $file->getBasename('.' . $ext),
                'path' => relative_file_path($file->getRealPath())
            ];
        }
        $this->assign([
            'role_options' => json_encode($auth_group, JSON_UNESCAPED_UNICODE),
            'avatar_list'  => json_encode($avatars, JSON_UNESCAPED_UNICODE)
        ]);
        return $this->fetch();
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        $create_data = json_decode($request->param('form'), true);
        $avatarFile = $request->file('avatarFile');
        $data = [
            'admin_name'         => $create_data['username'],
            'admin_pass'         => $create_data['password'],
            'admin_pass_confirm' => $create_data['confirmPassword'],
            'admin_email'        => $create_data['email'],
            'icon'               => $avatarFile,
            'admin_nickname'     => $create_data['nickname'],
            'status'             => $create_data['status'],
            '__token__'          => $request->param('__token__')
        ];
        // group_id字段不属于Admin验证模型，所以不能放在Admin.edit场景里
        $role_result = $this->validate(
            ['group_id' => $create_data['role']],
            ['group_id|角色' => 'integer']
        );
        $admin_result = $this->validate($data, 'Admin.create', [], true);
        $error_result = [];
        $error      = false;
        $errorMsg   = '更新失败，请再试一遍！';
        $url_success = $url_error = null;
        if (true !== $role_result)
            $error_result['group_id'] = $role_result;
        
        if (true !== $admin_result)
            $error_result = array_merge($error_result, $admin_result);
    
        $adminMode = model('Admin');
        // $adminMode->getOneData(['admin_nickname' => $data['admin_nickname']]);
        // 数据都验证无错了
        if (empty($error_result))
        {
            $avatar = $create_data['avatar'];
            // 如果有上传头像并验证通过，则重命名并移到指定目录
            if (null !== $avatarFile) 
            {
                // 头像目录
                $avatar_path = STATIC_PATH . 'AdminLTE' . DS . 'dist' . DS . 'img' . DS . 'avatars' . DS;
                $info = $avatarFile->rule(function($file) {
                    return pathinfo($file->getInfo('name'), PATHINFO_FILENAME) . '-' . date('YmdHis');
                })->move($avatar_path);
                if (false === $info) 
                {
                    $errorMsg = $avatarFile->getError();
                    $errorInfo = $avatarFile->getInfo('error');
                    $error_result = array_merge($error_result, [$errorInfo => $errorMsg]);
                    $error = true;
                }
                $avatar = $avatar_path . $info->getSaveName();
                // 绝对路径改成适合web显示的相对路径
                $avatar = relative_file_path($avatar);
            }
            
            $data['icon'] = $avatar;
            // 删除数据库里没有的字段
            unset($data['__token__'], $data['admin_pass_confirm']);

            if (!empty($data['admin_pass']))
            {
                $data['admin_pass'] = password_hash($data['admin_pass'], PASSWORD_DEFAULT);
            }
            // 新增管理员
            $admin = $adminMode->addData($data);
            // 管理员归属角色
            $authGroupAccessModel = model('AuthGroupAccess');
            $authGroupAccessModel->addData([
                'uid' => $admin->id,
                'group_id' => $create_data['role']
            ]);
            $url_success = url('admins/index');
        } else {
            $error = true;
        }
        // var_dump($error_result, $role_result, $admin_result);
        
        if ($error) 
        {
            $this->error('更新失败！', $url_error, $error_result);
        } else {
            $this->success('更新完成！', $url_success);
        }
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read(Request $request)
    {
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        if (1 != session('admin.group_id'))
        {
            // 普管员无权修改其他管理员信息
            if ($id != session('admin.id')) $this->error('非超管员无权修改其他管理员信息', null, '', 5);
        }
        
        $user = Db::name('admin')
            ->field('a.id, 
                    a.admin_name, 
                    a.admin_email, 
                    a.admin_nickname, 
                    a.create_time,
                    a.last_login_time, 
                    a.last_modified,
                    a.last_login_ip, 
                    a.icon, 
                    a.status, 
                    aga.group_id')
            ->alias('a')
            ->join('auth_group_access aga', 'a.id = aga.uid', 'LEFT')
            ->where('a.id', '=', $id)
            ->select();
        $auth_group_model = model('AuthGroup');
        $auth_group = $auth_group_model->getAllData();
        $this->assign([
            'user'         => session('admin'),
            'user_editing' => $user[0],
            'role_options' => json_encode($auth_group, JSON_UNESCAPED_UNICODE),
            'avatar_maxsize'  => $this->uploadLimit['avatar_maxsize'] // 最大允许的上传大小为约2M
        ]);
        return $this->fetch();
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request)
    {
        $field_name = $request->param('name');
        $uid        = $request->param('pk');
        $value      = $request->param('value');
        $error      = true;
        $errorMsg   = '更新失败，请再试一遍！';
        $url_success = $url_error = null;

        // 非超管员修改信息受限
        if (1 != session('admin.group_id'))
        {
            // 普管员不能改其他管理员
            if ($uid != session('admin.id')) $this->error('非超管员无权修改其他管理员信息');
            // 如果在改角色
            if ('ag_id' == $field_name)
            {
                // 普管员不能改角色组
                if ($value != session('admin.group_id')) $this->error('非超管员无权修改角色组');
            }
        }
        $adminLogic = model('Admin', 'logic');
        switch ($field_name) {
            // 更新用户的角色
            case 'ag_id':
                // update更新不支持模型验证
                $result = $this->validate(
                    [
                        'group_id' => $value
                    ],
                    ['group_id|角色' => 'integer']
                );
                
                if (true !== $result)
                {
                    $errorMsg = $result;
                } else {
                    $authGroupAccessModel = model('AuthGroupAccess');
                    $authGroupAccess = $authGroupAccessModel->editData(
                        ['group_id' => $value],
                        ['uid' => $uid]
                    );
                    $authGroupAccess && $error = false;
                }
                break;
            // 更新用户的头像
            case 'icon':
                $result = $adminLogic->updateAdmin(
                    ['id' => $uid],
                    ['icon' => $value],
                    ['icon|头像' => 'require']
                );
                break;
            // 更新用户的昵称
            case 'admin_nickname':
                $result = $adminLogic->updateAdmin(
                    ['id' => $uid],
                    ['admin_nickname' => $value]
                );
                break;
            // 更新用户的状态
            case 'status':
                $result = $adminLogic->updateAdmin(
                    ['id' => $uid],
                    ['status' => $value]
                );
                break;
            // 更新用户的邮箱
            case 'admin_email':
                $result = $adminLogic->updateAdmin(
                    ['id' => $uid],
                    ['admin_email' => $value],
                    ['admin_email|邮箱' => 'email']
                );
                break;
            case 'all':
                $edit_data = json_decode($value, true);
                $avatar = $request->file('avatar');
                $group_id = $edit_data['role']['selected'];
                $data = [
                    'admin_name'     => $edit_data['username'],
                    'admin_pass'     => $edit_data['password'],
                    'admin_email'    => $edit_data['email'],
                    'icon'           => $avatar,
                    'admin_nickname' => $edit_data['nickname'],
                    'status'         => $edit_data['status'],
                    '__token__'      => $request->param('__token__')
                ];
                // group_id字段不属于Admin验证模型，所以不能放在Admin.edit场景里
                $role_result = $this->validate(
                    ['group_id' => $group_id],
                    ['group_id|角色' => 'integer']
                );
                $admin_result = $this->validate($data, 'Admin.edit', [], true);
                $error_result = [];
                if (true !== $role_result)
                    $error_result['group_id'] = $role_result;
                
                if (true !== $admin_result)
                    $error_result = array_merge($error_result, $admin_result);
                
                // 数据都验证无错了
                if (empty($error_result))
                {
                    $result = [
                        'errorMsg' => '',
                        'error' => false
                    ];
                    // 如果有头像并验证通过，则重命名并移到指定目录
                    if (null !== $avatar) 
                    {
                        $info = $avatar->rule(function($file) {
                            return pathinfo($file->getInfo('name'), PATHINFO_FILENAME) . '-' . date('YmdHis');
                        })->move(STATIC_PATH . 'AdminLTE' . DS . 'dist' . DS . 'img' . DS . 'avatars' . DS);
                        if (false === $info) 
                        {
                            $result = [
                                'errorMsg' => $avatar->getError(),
                                'error' => true
                            ];
                        }
                    }
                    // 空的数据会覆盖掉数据库里原来有的数据，所以需删除值为空的数据
                    $data = array_filter($data, function ($value, $key) {
                        // 数据库里没有这一字段
                        if ($key == '__token__' ) return false;
                        if (empty($value)) {
                            // 'status'字段的值为'0'或0是合法的，不能过滤掉
                            if ($key == 'status' && ($value == '0' || $value == 0))
                            {
                                return true;
                            } else {
                                return false;
                            }
                        }
                        return true;
                    }, ARRAY_FILTER_USE_BOTH);

                    if (!empty($data['admin_pass']))
                    {
                        $data['admin_pass'] = password_hash($data['admin_pass'], PASSWORD_DEFAULT);
                    }
                        
                    // 更新管理员信息
                    $adminLogic->updateAdmin(['id' => $uid], $data);
                    // 更新管理员所属角色
                    $authGroupAccessModel = model('AuthGroupAccess');
                    $authGroupAccess = $authGroupAccessModel->editData(
                        ['group_id' => $group_id],
                        ['uid' => $uid]
                    );
                } else {
                    $result = [
                        'errorMsg' => $error_result,
                        'error' => true
                    ];
                }
                break;
        }
        
        if (!empty($result))
        {
            $error = $result['error'];
            $errorMsg = $result['errorMsg'];
        }
        
        if ($error) 
        {
            $this->error('更新失败！', $url_error, $errorMsg);
        } else {
            $this->success('更新完成！', $url_success);
        }
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id, Request $request)
    {
        if ($request->isDelete())
        {
            $error    = false;
            $group_id = session('admin.group_id');
            $msg      = [];
            // 非超管员无权删除
            if (1 == $group_id)
            {
                $adminMode = model('Admin');
                $admin = $adminMode->deleteData(['id' => $id]);
                if (!$admin)
                {
                    $error = true;
                    $msg[] = "请刷新页面，稍后再试！";
                } else {
                    $authGroupAccessMode = model('AuthGroupAccess');
                    $authGroupAccessMode->deleteData(['uid' => $id]);
                    $msg[] = "ID {$id}管理员已删除！";
                }
            } else {
                $error = true;
                $msg[] = '非超级管理员无权删除!';
            }

            if ($error)
            {
                // 第一个元素相当于错误信息title
                array_unshift($msg, '删除失败！');
                $this->error($msg);
            } else {
                $this->success($msg);
            }
        }
    }
}
