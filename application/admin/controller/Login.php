<?php
/**
 * 后台用户页面
 * 
 * 处理用户登录、注销
 */
namespace app\admin\controller;

use think\Controller as ThinkController;

/**
 * 后台用户登录
 */
class Login extends ThinkController
{
    protected $beforeActionList = ['isLogin'];
    /**
     * 检查用户是否已登录
     */
    function isLogin()
    {
        $admin_id = session('admin.id');
        if ($admin_id)
        {
            // 在用户已登录情况下，如果是表单提交就先提示再跳转
            if (request()->isPost())
            {
                $this->error(lang('LOGGED_ERROR'), 'index/index');
            }

            $this->redirect('index/index');
        }
    }

    /**
     * 显示登录页
     */
    function index() 
    {
        // trace(session('admin'));
        $this->assign('captcha', tr_enabled_admin_captcha());

        return $this->fetch();
    }

    /**
     * 处理登录验证
     */
    function doLogin()
    {
        $login = model('Admin', 'logic');
        
        $login->checkLogin();
    }
}