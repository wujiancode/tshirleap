<?php
/**
 * 角色组和权限管理
 * 
 * 角色组和权限的列表展示、新增、编辑、删除
 */
namespace app\admin\controller;

use think\Request;

class AuthGroup extends AdminBase
{
    private $model;
    private $modelLogic;
    protected $beforeActionList = [
        'setModelInstance'
    ];

    /**
     * 实例化当前控制器的模型
     */
    protected function setModelInstance()
    {
        $this->model      = model('AuthGroup');
        $this->modelLogic = model('AuthGroup', 'logic');
    }

    /**
     * 显示角色组列表
     *
     * @return \think\Response
     */
    public function index(Request $request)
    {
        // 获取所有角色组
        $authGroup = $this->model->getAllData(['id']);
        if (false !== $authGroup)
        {
            // 将结果集JSON序列化，以供前端使用
            $authGroup = $authGroup->hidden(['rules'])->toJson();
        }
        
        $this->assign([
            'authGroup' => $authGroup
        ]);
        return $this->fetch();
    }

    /**
     * 显示创建角色组单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        return $this->fetch();
    }

    /**
     * 保存新建的角色组
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        if ($request->isPost()) {
            $success_url    = null;
            $result = $this->modelLogic->formValidation('create');
            if ('success' === $result[0])
            {
                add_message($result, '', '新增成功！', 'success');
                $data = $this->modelLogic->getFormData();
                // 新增角色组
                $this->model->addData($data);
                $success_url = url('index');
            } else {
                add_message($result, '', '新增失败！', 'success');
            }
            
            if ('error' === $result[0])
            {
                $this->error($result[1]);
            } else {
                $this->success($result[1], $success_url);
            }
        }
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        echo 'read ' . $id;
    }

    /**
     * 显示编辑角色组单页.
     *
     * @param  int  $id 角色组ID
     * @return \think\Response
     */
    public function edit($id)
    {
        // 判断当前管理员是否是超管，如果不是则:
        if (1 != session('admin.group_id'))
        {
            // 判断当前管理员是否编辑自己角色组，如果不是则弹出提示并返回
            if ($id != session('admin.group_id')) 
            {
                $msg = '普通管理员无权修改其他角色组';
                $this->error($msg);
            }
        }
        // 获取当前编辑的角色组信息
        $authGroupModel = model('auth_group');
        $authGroup = $authGroupModel->getOneData($id);
        if ($authGroup)
            $authGroupJson = $authGroup->visible(['id', 'title', 'status', 'description'])->toJson();
        else
            // JSON.parse('')解析要求JSON字符串，所以不能赋空值
            $authGroupJson = '""';
        $this->assign([
            'authGroup' => $authGroupJson
        ]);

        return $this->fetch();
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->isPut()) {
            $scene          = $request->param('scene');
            $success_url    = null;
            $data           = $this->modelLogic->getFormData();
            // 角色组状态更改场景
            if ( 'status' === $scene )
            {
                $error_title = '更新失败！';
                $msg_result  = $this->modelLogic->checkStatusPermission($id, $data['status'], [], $error_title);

                if ( empty($msg_result) ) {
                    $msg_result = $this->modelLogic->formValidation('', ['status|状态' => 'integer']);
                    if ('success' === $msg_result[0])
                    {
                        // 更新角色组状态
                        $this->model->editData($data, ['id' => $id]);
                    }
                }
            } // 角色组编辑场景 
            else if ('all' === $scene) {
                $msg_result = $this->modelLogic->formValidation('create');
                if ('success' === $msg_result[0])
                {
                    // 更新角色组
                    $this->model->editData($data, ['id' => $id]);
                    $success_url = url('index');
                }
            }
            
            if ('error' === $msg_result[0])
            {
                $this->error($msg_result[1]);
            } else {
                $this->success($msg_result[1], $success_url);
            }
        }
    }

    /**
     * 删除指定角色组
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id, Request $request)
    {
        if ($request->isDelete())
        {
            $msg_result = [];
            $authGroup = $this->model->getOneData($id);
            $result = $this->model->deleteData(['id' => $id]);
            if (!$result)
            {
                add_message($msg_result, '请稍后再试！', '删除失败');
            } else {
                $authGroupAccessMode = model('AuthGroupAccess');
                $authGroupAccessMode->deleteData(['group_id' => $id]);
                add_message($msg_result, '', $authGroup->title . '已删除！', 'success');
            }

            if ('error' === $msg_result[0])
            {
                $this->error($msg_result[1]);
            } else {
                $this->success($msg_result[1]);
            }
        }
    }

    /**
     * 显示编辑角色组权限单页.
     *
     * @param  int  $id 角色组ID
     * @return \think\Response
     */
    public function auth($id)
    {
        // 超级角色组拥有所有权限，所以不要编辑其权限
        if ('1' === $id) $this->error('超管员角色组的权限不能被编辑');
        // 获取当前用户组规则
        $rules = $this->model->getValueData($id, 'rules');
        $authRuleModel = model('AuthRule');
        $names = $authRuleModel->getColumnData(
            [
                'id' => ['in', $rules]
            ], 
            'name'
        );
        $status = $authRuleModel->getColumnData([], 'name, status');
        // 格式化菜单数据(menuData)，使之符合前台要求的数据格式
        $menu_data = menu_data_recursive($this->menuData, $names, $status);
        // 获取所有用户组规则对应的菜单ID
        $deepest_id_checked = get_deepest_id_checked($menu_data);
        $deepest_id_checked = json_encode( explode(',', $deepest_id_checked), JSON_NUMERIC_CHECK );
        $this->assign([
            'menuData' => json_encode($menu_data, JSON_UNESCAPED_UNICODE),
            'checked' => $deepest_id_checked,
            'group_id' => $id
        ]);

        return $this->fetch();
    }

    /**
     * 保存更新的角色组权限
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function authUpdate(Request $request, $id)
    {
        // 超级角色组拥有所有权限，所以不要编辑其权限
        if ('1' === $id) $this->error('超管员角色组的权限不能被编辑');
        if ($request->isPut()) {
            $keys = json_decode($request->param('keys'), true);
            // 将菜单ID转换成菜单的'控制器/操作'
            $hrefs = get_href_checked($this->menuData, $keys);
            $authRuleModel = model('AuthRule');
            // 将菜单'控制器/操作'转换成规则ID
            $names = $authRuleModel->getColumnData(
                [
                    'name' => ['in', $hrefs]
                ], 
                'id'
            );
            $rules = implode(',', $names);
            $this->model->editData(['rules' => $rules], ['id' => $id]);
            
            $this->success('修改成功！', url('index'));
        }
    }
}
