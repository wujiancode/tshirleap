<?php
namespace app\admin\controller;

use think\Controller as ThinkController;
use category\CategoryContext;
use category\CategoriesGeneral;
use Library\ORG\Util\Auth;
use think\Lang;

/**
 * 后台基础控制器
 */
class AdminBase extends ThinkController
{
    /**
     * 菜单数据
     * 
     * @var array
     */
    protected $menuData;
    /**
     * 各种最大允许的上传大小
     * 
     * @var array
     */
    protected $uploadLimit = [
        // 头像的最大允许的上传大小为2M
        'avatar_maxsize' => 2*1024 * 1024
    ];
    /**
     * 后台控制器初始化
     */
    protected function _initialize()
    {
        // 加载当前控制器语言
        Lang::load(
            APP_PATH . 
            MODULE . DS . 
            'lang' . DS . 
            LANGUAGE . DS . 
            CONTROLLER . '.php'
        );
        $admin_id = session('admin.id');
        // 如果还没登录，则回到登录页去
        if (!$admin_id)
        {
            // 如果用户是在后台编辑、设置等操作就先提示再跳转
            if (request()->isPost())
            {
                $this->error(lang('NOT_LOGIN'), 'login/index');
            }

            $this->redirect('login/index');
        }
      
        // 如果不是超级管理员，则需要权限认证
        if (1 != $admin_id)
        {
            $rule = str_lower_to_camel(CONTROLLER) . '/' . ACTION;
            // 如果当前控制器/方法不在白名单中，则用Auth认证
            if ( !in_access_whitelist($rule) )
            {
                if ( !check_auth_rule($rule, $admin_id) ) $this->error('你没有访问权限');
            }
        }
        // 获取用户头像，没有就用默认
        $avatar    = session('admin.icon') ?: '/tshirleap/public/static/AdminLTE/dist/img/avatar5.png';
        $user_name = session('admin.nickname') ?: session('admin.name');
        $this->assign('avatar', $avatar);
        $this->assign('user_name', $user_name);
        // cache('menu_data', null);
        $this->menuData = cache('menu_data');
        $this->menuDataByStatus = cache('menu_data_by_status');
        if (!$this->menuData)
        {
            // 生成左侧菜单
            $menus_model = model('AdminMenus', 'logic');
            // 获取所有菜单数据
            $this->menuData = $menus_model->getMenuData();
            // 获取所有状态值为1的菜单数据
            $this->menuDataByStatus = $menus_model->getMenuData(1);
            cache('menu_data', $this->menuData);
            cache('menu_data_by_status', $this->menuDataByStatus);
        }
        
        $menus = new CategoryContext(new CategoriesGeneral($this->menuDataByStatus));
        $this->assign('menus', $menus->getResult());
        $this->assign('menu_data', $this->menuDataByStatus);
        
        // 痕迹导航
        $this->assign('breadcrumb', array(
            'title' => lang('NAVBAR_TITLE'),
            'url'   => url(),
        ));
    }
}