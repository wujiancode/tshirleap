<?php
// var_dump(url('/'));
//配置文件
return [
    // 后台错误模板文件
    'dispatch_admin_error_tmpl' => 'public' . DS . 'error',
    'session' => [
        'id'             => '',
        // SESSION_ID的提交变量,解决flash上传跨域
        'var_session_id' => '',
        // SESSION 前缀
        'prefix'         => 'think',
        // 驱动方式 支持redis memcache memcached
        'type'           => '',
        // 是否自动开启 SESSION
        'auto_start'     => true,
        // 只能通过HTTP协议访问
        'httponly'       => true,
        // 'type'           => 'redis'
    ],
];