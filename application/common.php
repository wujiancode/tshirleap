<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件

/**
 * 文件的路径以Unix风格相对化
 * 
 * @param string|array $files 要被处理的文件路径
 * @return string|array 返回文件的相对路径
 */
function relative_file_path($files)
{
    // 获取项目Unix风格的绝对对路径
    $project_path = str_replace(['/', '\\'], '/', dirname(realpath(APP_PATH)));
    $project_path = dirname($project_path);
    // 替换掉文件的项目路径部分
    if (is_array($files))
    {
        $file_path = [];
        foreach ($files as $file) {
            $file = str_replace(['/', '\\'], '/', realpath($file));
            $file_path[] = str_replace($project_path, '', $file);
        }
    } else if (is_string($files)) {
        $files = str_replace(['/', '\\'], '/', realpath($files));
        $file_path = str_replace($project_path, '', $files);
    }
    return $file_path;
}

/**
 * 获取当前控制器对应的分类(菜单)ID路径，
 * 其路径格式：一级类ID-二级类ID...(1-2-...)
 * @param array $category 分类数据数组
 * @return string 返会分类(菜单)ID路径
 */
function get_category_path($category, $parent_id = 0, $parent_cid = '')
{
    $cid_path = '';
    if ( !empty($category[$parent_id]) )
    {
        foreach ($category[$parent_id] as $id => $sub_category) 
        {
            $category_link = $parent_cid . $id;
            
            if (false !== strpos($sub_category['href'], str_lower_to_camel(CONTROLLER) . '/') )
            {
                $cid_path = $category_link;
            }
            if (!empty($category[$id]) )
            {
                $cid_path = get_category_path( $category, $id, $category_link . '-' );
            }
            // 获得想要的数据后立即结束递归
            if ( !empty($cid_path) ) return $cid_path;
        }
    }
    return $cid_path;
}

/**
 * 将驼峰式的字符串转成用下划线连接的小写字符串
 * 
 * @param string $string
 * @return string 转成xxx_xxx格式的字符串
 */
function str_camel_to_lower($string)
{
    if ( !empty($string) )
    {
        $string = preg_split('/(?<!^)(?=[A-Z])/', $string);
        $string = array_reduce($string, function ($carry, $item) {
            return strtolower($carry) . '_' . strtolower($item);
        });
        $string = ltrim($string, '_');
    } 
    
    return $string;
}

/**
 * 将小写字符串转成首字母大写的驼峰式的字符串
 * 
 * @param string $string
 * @return string 转成XxxYyy格式的字符串
 */
function str_lower_to_camel($string)
{
    $result = '';
    if ( !empty($string) )
    {
        $result = strtolower($string);
        $result = ucwords($result, '_');
        if (false !== strpos($result, '_'))
        {
            $result = strtr($result, ['_' => '']);
        }
    }

    return $result;
}

/**
 * 添加成功和错误消息
 * 
 * @param array $result 消息容器
 * @param string|array $message 消息,如果是数组，则需是数字索引
 * @param string $title 消息标题
 * @param string $type 消息类型，success：成功消息，error：失败消息
 * @return void
 */
function add_message(array &$result, $message = '', $title = '', $type = 'error')
{
    // // 如果传入的$result是字符串，则$result[0]依然是字符串
    // if ( !is_array($result) ) return false;

    $result[0] = $type;
    if ('success' === $type)
    {
        if ( empty($title) ) $title = '成功！';
    } else if ('error' === $type) {
        if ( empty($title) ) $title = '错误！';
    }
    // 第一个元素相当于消息title
    $result[1][0] = $title;
    
    if ( !empty($message) )
    {
        if ( is_array($message) )
        {
            // 确保$message是数字索引的数组
            $message = array_values($message);
            $msg_array = array_merge($result[1], $message);
            $result[1] = $msg_array;
        } else {
            $result[1][] = $message;
        }
    }
}