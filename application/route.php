<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\Route;

// 注册路由到验证吗控制器
Route::get('new_captcha', "@common/captcha_new/index");
// 注册管理员管理的资源路由
// Route::rest('index',['GET', '/index','index']);
Route::resource('admins','admin/admins');
Route::get('admin/admins/index', 'admin/admins/index');
// 注册角色组的资源路由
Route::rest('auth',['GET', '/:id/auth','auth']);
Route::rest('authUpdate',['PUT', '/:id/update','authUpdate']);
Route::resource('auth_group','admin/AuthGroup');
Route::get('admin/auth_group/index', 'admin/AuthGroup/index');

return [
    '__pattern__' => [
        'name' => '\w+',
    ],
    '[hello]'     => [
        ':id'   => ['index/hello', ['method' => 'get'], ['id' => '\d+']],
        ':name' => ['index/hello', ['method' => 'post']],
    ],

];

